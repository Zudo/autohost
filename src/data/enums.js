const SERIALISE_TYPES = {
    "AUTOHOST": 0,
    "TOURNAMENT": 1
}

const TWO_PLAYER_MODES = {
    "STATIC_HOTSEAT": 0,
    "DYNAMIC_HOTSEAT": 1
};

const APM_LIMIT_EXEMPTIONS = {
    "NONE": 0,
    "RANKED": 1
};

const TOURNAMENT_TYPES = {
    "SINGLE_ELIMINATION": "single elimination",
    "DOUBLE_ELIMINATION": "double elimination",
    "ROUND_ROBIN": "round robin"
};

module.exports = {SERIALISE_TYPES, TWO_PLAYER_MODES, APM_LIMIT_EXEMPTIONS, TOURNAMENT_TYPES};
