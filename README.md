## ⚠⚠⚠ BIG SCARY DISCLAIMER ⚠⚠⚠

The source code is here to allow you to contribute to development and (perhaps) to learn. It's **NOT** for making
unauthorised bots. Ask osk for permission before connecting to TETR.IO with anything other than the official client.

Also, don't make a request for a bot account without properly thinking it through first. Don't waste staff time please!

**You will be banned from the game if you don't follow the rules. It's not my fault if that happens.**

# Autohost for TETR.IO

A bot for osk's TETR.IO that allows for the following additional features in custom games:

* Automatic game starts
* 1v1 queues
* Participation requirements (rank and level)
* Room moderation (including bans)
* Custom game presets
* And more to come!

This is built on a custom Ribbon implementation in Node.js.

## Need help?

Check out the [wiki](https://github.com/ZudoB/autohost/wiki) for guides on how to use Autohost.

## Found a bug?

Please open an [issue](https://github.com/ZudoB/autohost/issues/new) or submit a pull request. Alternatively, send me a
message on Discord (`Zudo#0800`)

## Want to support the project?

**Please consider [supporting TETR.IO](https://tetr.io/#S:) instead** - osk deserves your money more than I do. However,
I do have a [Ko-fi](https://ko-fi.com/zudobtw) if you would like to help with hosting costs.

## Acknowledgements

Special thanks goes out to Poyo for [documenting the Ribbon](https://github.com/Poyo-SSB/tetrio-bot-docs), Zyrixia for the in game avatar, craftxbox for
the initial idea, and to the many people who have helped test Autohost.
